provider "aws" {
  region = var.region 
}

resource "aws_instance" "Task" {
    ami = var.ami 
    instance_type = var.instance_type
    key_name = var.key_name
    vpc_security_group_ids = var.vpc_security_group_ids
    count = var.count
    tags = var.tags
  
}
terraform {
  backend "s3" {
    bucket = "tf-backend"
    key = "terraform.tfstate"
    region = "value"
    
  }
}

variable "region" {
    default = "value"
    description = "for crete instance in region"
}

variable "ami" {
    default = "value"
    description = "ami for create instance"
}

variable "instance_type" {
    default = "value"
    description = "instance ttyoe for create instance"
}

variable "key_name" {
    default = "value"
    description = "key for get access for instance CLI"

}

variable "vpc_security_group_ids" {
    default = "value"
    description = "security group for instances"  
}

variable "count" {
    default = "5"
    description = "count of create instance"
}

variable "tags" {
    default = "Task"
    description = "tags for instance"
}
